package org.alliancegenome.api.dto;

import java.util.Date;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class DataSetDTO extends BaseDTO {

	public Long id;
	public String name;
	public String metaData;
	public Date injectdate;

	public DataSetDTO() { }
	
	public DataSetDTO(String dataSetName) {
		this.name = dataSetName;
	}
	
	public String toString() {
		return "{id: " + id + ", name: " + name + "}";
	}

	@Override
	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	@Override
	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		return true;
	}
}
