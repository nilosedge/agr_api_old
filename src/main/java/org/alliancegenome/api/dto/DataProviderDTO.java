package org.alliancegenome.api.dto;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class DataProviderDTO extends BaseDTO {

	public Long id;
	public String name;
	public String metaData;
	
	public DataProviderDTO() { }
	
	public DataProviderDTO(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "{id: " + id + ", name: " + name + "}";
	}

	@Override
	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	@Override
	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		return true;
	}
}
