package org.alliancegenome.api.dto;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
	description="Location of a sequence object used both by display, and by jbrowse to render a browser."
)
public class GenomeLocationDTO extends BaseDTO {
	
	@ApiModelProperty(value="The assembly on which the position and chromosome are based.", position=2)
	public String assembly;
	@ApiModelProperty(value="the start position of the location.", position=3)
	public Integer startPosition;
	@ApiModelProperty(value="the end position of the location.", position=4)
	public Integer endPosition;
	@ApiModelProperty(value="the chromosome of the genomic feature.  Note: whatever loaded here, will be used in JBrowse.", position=5)
	public String chromosome;
	@ApiModelProperty(value="strand of the genome location.", position=6)
	public String strand;

	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		// "required": [ "assembly","chromosome" ],
		return true;
	}
}
