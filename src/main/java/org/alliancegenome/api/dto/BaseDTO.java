package org.alliancegenome.api.dto;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

public abstract class BaseDTO {
	public abstract void Accept(DTOVisitorInterface pi);
	public abstract boolean isValid();
}
