package org.alliancegenome.api.dto;

import java.util.ArrayList;
import java.util.List;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;
import org.alliancegenome.api.util.GeneVisitor;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
		description="An entry with Gene information from a MOD.  Gene set includes: genes, psuedogenes and not-protein coding genes.  It does not include engineered foreign genes, transcripts or other features."
)
public class GeneDTO extends BaseDTO {
	
	@ApiModelProperty(value="The primary (MOD) ID for an entity.", position=1)
	public String primaryId;
	@ApiModelProperty(value="The name of the entity.", position=2)
	public String name;
	@ApiModelProperty(value="The taxon id of the species of the entity. AGR currently supports the ids listed below.", position=3)
	public String taxonId;
	@ApiModelProperty(value="The symbol for the gene entity.", position=4)
	public String symbol;
	@ApiModelProperty(value="Human readable gene description or synopsis", position=5)
	public String geneSynopsis;
	@ApiModelProperty(value="URL to a gene synopsis outside the MOD, like wikipedia.  Per request from BGI-WG.", position=6)
	public String geneSynopsisUrl;
	@ApiModelProperty(value="URL to MOD page for showing pubs associated with gene entity.", position=7)
	public String geneLiteratureUrl;
	@ApiModelProperty(value="The SO Term that represents as best we can, the bioType, or featureType of the object in the file.  DQMs agree that this should be a standardized set.", position=8)
	public String soTermId;
	
	@ApiModelProperty(value="List of Synonyms for the Gene", position=9)
	public List<String> synonyms = new ArrayList<String>();
	
	@ApiModelProperty(value="Collection holding a limited set (for now) of database cross references for each gene.  That set is defined in geneCrossReferences.json dataSoruce enumeration.  NCBI GENE means just the NCBI Gene reference.  UniProtKB is swissprot and trembl.  Ensembl is just the GENE id for now (not transcript nor protein).  Vega was eliminated as it did not apply to all organisms in the repository.", position=10)
	public List<CrossReferenceDTO> crossReferences = new ArrayList<CrossReferenceDTO>();
	
	@ApiModelProperty(value="Collection holding the set of locations for each gene object.", position=11)
	public List<GenomeLocationDTO> genomeLocations = new ArrayList<GenomeLocationDTO>();
	
	@ApiModelProperty(value="Collection of MOD-specific IDs that have been merged into gene entity.", position=12)
	public List<String> secondaryIds = new ArrayList<String>();
	
	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	public String toString() {
		GeneVisitor v = new GeneVisitor();
		Accept(v);
		return v.generateOutput();
	}

	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		// "required": [ "primaryId", "taxonId", "symbol", "soTermId" ],
		return true;
	}
}
