package org.alliancegenome.api.dto;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
		description="An crossReference entity (e.g.: NCBIGENE links, UniProt Links and links back to MODs)."
)
public class CrossReferenceDTO extends BaseDTO {

	@ApiModelProperty(value="The primary identifier of the related resource.", position=1)
	public String id;
	@ApiModelProperty(value="Constraints the dataProvider to conform to the dataProvider schema.  This schema enforces that a dataProvider provide the taxonId of the record being added, as well as the database source.  see dataProvider.json for more information. An standard set of information regarding data source and taxon ids for the AGR. The MOD, human, or other source of data. AGR currently supports the entities listed below. \"FB\", \"MGI\", \"RGD\", \"SGD\", \"ZFIN\", \"WB\", \"DRSC\", \"NCBIGene\", \"UniProtKB\", \"Ensembl\", \"RNAcentral\", \"HGNC\"", position=2)
	public String dataProvider;

	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		// "required": [ "dataProvider","id"],
		return true;
	}
}

