package org.alliancegenome.api.dto;

import org.alliancegenome.api.interfaces.DTOVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

public class DataEntryDTO extends BaseDTO {

	public Long id;
	public String primaryId;
	public String secondaryId;
	public String tertiaryId;
	public String data;
	
	public DataEntryDTO() { }
	
	public String toString() {
		return "{primaryId: " + primaryId + ", data: " + data + "}";
	}

	@Override
	public void Accept(DTOVisitorInterface pi) {
		pi.Visit(this);
	}

	@Override
	@ApiModelProperty(required=false) @JsonIgnore
	public boolean isValid() {
		return true;
	}
}
