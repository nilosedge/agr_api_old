package org.alliancegenome.api.util;

import org.alliancegenome.api.domainmodels.CrossReference;
import org.alliancegenome.api.domainmodels.DataEntry;
import org.alliancegenome.api.domainmodels.DataProvider;
import org.alliancegenome.api.domainmodels.DataSet;
import org.alliancegenome.api.domainmodels.Gene;
import org.alliancegenome.api.domainmodels.GenomeLocation;
import org.alliancegenome.api.domainmodels.SecondaryId;
import org.alliancegenome.api.domainmodels.Synonym;
import org.alliancegenome.api.dto.CrossReferenceDTO;
import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.api.dto.GenomeLocationDTO;
import org.alliancegenome.api.interfaces.DTOVisitorInterface;
import org.alliancegenome.api.interfaces.ModelVisitorInterface;

public class GeneVisitor extends PrinterUtil implements DTOVisitorInterface, ModelVisitorInterface {

	@Override
	public void Visit(GenomeLocationDTO genomeLocation) {
		printi("<GenomeLocation>");
		if(genomeLocation.assembly != null) printiu("<Assembly>" + genomeLocation.assembly + "</Assembly>");
		if(genomeLocation.chromosome != null) printiu("<Chromosome>" + genomeLocation.chromosome + "</Chromosome>");
		if(genomeLocation.strand != null) printiu("<Strand>" + genomeLocation.strand + "</Strand>");
		if(genomeLocation.startPosition != null) printiu("<StartPosition>" + genomeLocation.startPosition + "</StartPosition>");
		if(genomeLocation.endPosition != null) printiu("<EndPosition>" + genomeLocation.endPosition + "</EndPosition>");
		printu("</GenomeLocation>");
	}

	@Override
	public void Visit(CrossReferenceDTO crossReference) {
		printi("<CrossReference>");
		printiu("<Id>" + crossReference.id + "</Id>");
		printiu("<DataProvider>" + crossReference.dataProvider + "</DataProvider>");
		printu("</CrossReference>");
	}

	@Override
	public void Visit(GeneDTO gene) {

		printi("<Gene>");

		if(gene.primaryId != null) printiu("<PrimaryId>" + gene.primaryId + "</PrimaryId>");
		if(gene.geneLiteratureUrl != null) printiu("<GeneLiteratureUrl>" + gene.geneLiteratureUrl + "</GeneLiteratureUrl>");
		if(gene.geneSynopsis != null) printiu("<GeneSynopsis>" + gene.geneSynopsis + "</GeneSynopsis>");
		if(gene.geneSynopsisUrl != null) printiu("<GeneSynopsisUrl>" + gene.geneSynopsisUrl + "</GeneSynopsisUrl>");
		if(gene.name != null) printiu("<Name>" + gene.name + "</Name>");
		if(gene.soTermId != null) printiu("<SoTermId>" + gene.soTermId + "</SoTermId>");
		if(gene.symbol != null) printiu("<Symbol>" + gene.symbol + "</Symbol>");
		if(gene.taxonId != null) printiu("<TaxonId>" + gene.taxonId + "</TaxonId>");

		if(gene.synonyms.size() > 0) {
			printi("<Synonyms>");
			for(String s: gene.synonyms) {
				printiu(s);
			}
			printu("</Synonyms>");
		}

		if(gene.secondaryIds.size() > 0) {
			printi("<SecondaryIds>");
			for(String s: gene.secondaryIds) {
				printiu(s);
			}
			printu("</SecondaryIds>");
		}

		if(gene.crossReferences.size() > 0) {
			printi("<CrossReferences>");
			for(CrossReferenceDTO crm: gene.crossReferences) {
				crm.Accept(this);
			}
			printu("</CrossReferences>");
		}

		if(gene.genomeLocations.size() > 0) {
			printi("<GenomeLocations>");
			for(GenomeLocationDTO glm: gene.genomeLocations) {
				glm.Accept(this);
			}
			printu("<GenomeLocations>");
		}

		printu("</Gene>");
	}

	@Override
	public void Visit(DataEntryDTO dataEntry) {
	}

	@Override
	public void Visit(DataProviderDTO dataProvider) { }

	@Override
	public void Visit(DataSetDTO dataSet) { }

	
	
	@Override
	public void Visit(GenomeLocation genomeLocation) {
		printi("<GenomeLocation>");
		if(genomeLocation.assembly != null) printiu("<Assembly>" + genomeLocation.assembly + "</Assembly>");
		if(genomeLocation.chromosome != null) printiu("<Chromosome>" + genomeLocation.chromosome + "</Chromosome>");
		if(genomeLocation.strand != null) printiu("<Strand>" + genomeLocation.strand + "</Strand>");
		if(genomeLocation.startPosition != null) printiu("<StartPosition>" + genomeLocation.startPosition + "</StartPosition>");
		if(genomeLocation.endPosition != null) printiu("<EndPosition>" + genomeLocation.endPosition + "</EndPosition>");
		printu("</GenomeLocation>");
	}

	@Override
	public void Visit(CrossReference crossReference) {
		printi("<CrossReference>");
		printiu("<Id>" + crossReference.id + "</Id>");
		printiu("<DataProvider>" + crossReference.dataProvider + "</DataProvider>");
		printu("</CrossReference>");
	}

	@Override
	public void Visit(Gene gene) {

		printi("<Gene>");

		if(gene.primaryId != null) printiu("<PrimaryId>" + gene.primaryId + "</PrimaryId>");
		if(gene.geneLiteratureUrl != null) printiu("<GeneLiteratureUrl>" + gene.geneLiteratureUrl + "</GeneLiteratureUrl>");
		if(gene.geneSynopsis != null) printiu("<GeneSynopsis>" + gene.geneSynopsis + "</GeneSynopsis>");
		if(gene.geneSynopsisUrl != null) printiu("<GeneSynopsisUrl>" + gene.geneSynopsisUrl + "</GeneSynopsisUrl>");
		if(gene.name != null) printiu("<Name>" + gene.name + "</Name>");
		if(gene.soTermId != null) printiu("<SoTermId>" + gene.soTermId + "</SoTermId>");
		if(gene.symbol != null) printiu("<Symbol>" + gene.symbol + "</Symbol>");
		if(gene.taxonId != null) printiu("<TaxonId>" + gene.taxonId + "</TaxonId>");

		if(gene.synonyms.size() > 0) {
			printi("<Synonyms>");
			for(Synonym s: gene.synonyms) {
				s.Accept(this);
			}
			printu("</Synonyms>");
		}

		if(gene.secondaryIds.size() > 0) {
			printi("<SecondaryIds>");
			for(SecondaryId s: gene.secondaryIds) {
				s.Accept(this);
			}
			printu("</SecondaryIds>");
		}

		if(gene.crossReferences.size() > 0) {
			printi("<CrossReferences>");
			for(CrossReference crm: gene.crossReferences) {
				crm.Accept(this);
			}
			printu("</CrossReferences>");
		}

		if(gene.genomeLocations.size() > 0) {
			printi("<GenomeLocations>");
			for(GenomeLocation glm: gene.genomeLocations) {
				glm.Accept(this);
			}
			printu("<GenomeLocations>");
		}

		printu("</Gene>");
		
	}

	@Override
	public void Visit(Synonym synonym) {
		printiu("<Synonym>" + synonym.synonym + "</Synonym>");
	}

	@Override
	public void Visit(SecondaryId secondaryId) {
		printiu("<SecondaryId>" + secondaryId.secondaryId + "</SecondaryId>");
	}
	
	@Override
	public void Visit(DataEntry dataEntry) { }

	@Override
	public void Visit(DataProvider dataProvider) { }

	@Override
	public void Visit(DataSet dataSet) { }

}
