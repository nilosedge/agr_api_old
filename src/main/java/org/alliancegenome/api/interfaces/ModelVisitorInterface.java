package org.alliancegenome.api.interfaces;

import org.alliancegenome.api.domainmodels.CrossReference;
import org.alliancegenome.api.domainmodels.DataEntry;
import org.alliancegenome.api.domainmodels.DataProvider;
import org.alliancegenome.api.domainmodels.DataSet;
import org.alliancegenome.api.domainmodels.Gene;
import org.alliancegenome.api.domainmodels.GenomeLocation;
import org.alliancegenome.api.domainmodels.SecondaryId;
import org.alliancegenome.api.domainmodels.Synonym;

public interface ModelVisitorInterface {
	void Visit(GenomeLocation genomeLocation);
	void Visit(CrossReference crossReference);
	void Visit(Gene gene);
	void Visit(DataEntry dataEntry);
	void Visit(DataProvider dataProvider);
	void Visit(DataSet dataSet);
	void Visit(Synonym synonym);
	void Visit(SecondaryId secondaryId);
}
