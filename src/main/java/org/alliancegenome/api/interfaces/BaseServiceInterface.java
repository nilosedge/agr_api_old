package org.alliancegenome.api.interfaces;

import java.util.HashMap;
import java.util.List;

public interface BaseServiceInterface<T> {
	public T add(T model);
	public T update(T model);
	public List<T> get(HashMap<String, Object> searchFields);
	public T delete(Object key);
}
