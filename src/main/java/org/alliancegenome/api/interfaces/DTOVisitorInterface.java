package org.alliancegenome.api.interfaces;

import org.alliancegenome.api.dto.CrossReferenceDTO;
import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.api.dto.GenomeLocationDTO;

public interface DTOVisitorInterface {
	void Visit(GenomeLocationDTO genomeLocation);
	void Visit(CrossReferenceDTO crossReference);
	void Visit(GeneDTO gene);
	void Visit(DataEntryDTO dataEntry);
	void Visit(DataProviderDTO dataProvider);
	void Visit(DataSetDTO dataSet);
}
