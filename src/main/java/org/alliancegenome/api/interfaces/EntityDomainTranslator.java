package org.alliancegenome.api.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.alliancegenome.api.domainmodels.BaseDomainModel;
import org.alliancegenome.api.dto.BaseDTO;

public abstract class EntityDomainTranslator<T extends BaseDomainModel, E extends BaseDTO> {
	
	public T translate(E domain) {
		return domainToEntity(domain);
	}
	
	public E translate(T entity) {
		return entityToDomain(entity);
	}
	
	public List<T> translateDomains(List<E> domains) {
		ArrayList<T> entities = new ArrayList<T>();
		for(E domain: domains) {
			entities.add(domainToEntity(domain));
		}
		return entities;
	}
	
	public List<E> translateEntities(List<T> entities) {
		ArrayList<E> domains = new ArrayList<E>();
		for(T entity: entities) {
			domains.add(entityToDomain(entity));
		}
		return domains;
	}
	
	protected abstract E entityToDomain(T entity);
	protected abstract T domainToEntity(E domain);
	
}
