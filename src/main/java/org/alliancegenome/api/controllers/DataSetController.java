package org.alliancegenome.api.controllers;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.api.services.DataSetService;
import org.alliancegenome.common.interfaces.DataSetRESTInterface;

@Default
public class DataSetController implements DataSetRESTInterface {

	@Inject
	private DataSetService dataSetService;

	//private Logger log = Logger.getLogger(getClass());
	
	public DataSetDTO createDataSet(String api_access_token, Long dataProviderId, DataSetDTO ds) {
		return dataSetService.add(dataProviderId, ds);
	}

	public DataSetDTO updateDataSet(String api_access_token, DataSetDTO ds) {
		return dataSetService.update(ds);
	}

	public List<DataSetDTO> getDataSet(Long id, String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(id != null) { map.put("id", id); }
		if(name != null) { map.put("name", name); }
		return dataSetService.get(map);
	}

	public DataSetDTO deleteDataSet(String api_access_token, Long id) {
		return dataSetService.delete(id);
	}

}
