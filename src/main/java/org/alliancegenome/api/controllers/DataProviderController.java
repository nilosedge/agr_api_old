package org.alliancegenome.api.controllers;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.services.DataProviderService;
import org.alliancegenome.common.interfaces.DataProviderRESTInterface;
import org.apache.log4j.Logger;

@Default
public class DataProviderController implements DataProviderRESTInterface {

	@Inject
	private DataProviderService dataProviderService;
	
	private Logger log = Logger.getLogger(getClass());

	public DataProviderDTO createDataProvider(String api_access_token, DataProviderDTO dp) {
		log.info(dp);
		return dataProviderService.add(dp);
	}

	public DataProviderDTO updateDataProvider(String api_access_token, DataProviderDTO dp) {
		log.info(dp);
		return dataProviderService.update(dp);
	}

	public List<DataProviderDTO> getDataProvider(Long id, String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(id != null) { map.put("id", id); }
		if(name != null) { map.put("name", name); }
		return dataProviderService.get(map);
	}

	public DataProviderDTO deleteDataProvider(String api_access_token, Long id) {
		return dataProviderService.delete(id);
	}

}
