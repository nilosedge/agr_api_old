package org.alliancegenome.api.controllers;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.services.DataEntryService;
import org.alliancegenome.common.interfaces.DataEntryRESTInterface;
import org.apache.log4j.Logger;

@Default
public class DataEntryController implements DataEntryRESTInterface {

	@Inject
	private DataEntryService dataEntryService;
	
	private Logger log = Logger.getLogger(getClass());

	public DataEntryDTO createDataEntry(String api_access_token, Long dataSetId, DataEntryDTO ds) {
		return dataEntryService.add(ds, dataSetId);
	}

	public List<DataEntryDTO> createDataEntryBatch(String api_access_token, Long dataSetId, List<DataEntryDTO> ds) {
		return dataEntryService.addBatch(ds, dataSetId);
	}

	public DataEntryDTO updateDataEntry(String api_access_token, DataEntryDTO ds) {
		return dataEntryService.update(ds);
	}

	public List<DataEntryDTO> getDataEntry(Long id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(id != null) { map.put("id", id); }
		log.info("DataEntryController.getDataEntryById: " + map);
		return dataEntryService.get(map);
	}

	public DataEntryDTO deleteDataEntry(String api_access_token, Long id) {
		return dataEntryService.delete(id);
	}

}
