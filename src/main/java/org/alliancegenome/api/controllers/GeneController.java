package org.alliancegenome.api.controllers;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.api.services.GeneService;
import org.alliancegenome.common.interfaces.GeneRESTInterface;

@Default
public class GeneController implements GeneRESTInterface {
	
	@Inject
	private GeneService geneService;
	
	//private Logger log = Logger.getLogger(getClass());

	public GeneDTO createGene(String api_access_token, GeneDTO gene) {
		return geneService.add(gene);
	}

	public List<GeneDTO> createGeneBatch(String api_access_token, List<GeneDTO> genes) {
		return geneService.addBatch(genes);
	}

	public GeneDTO updateGene(String api_access_token, GeneDTO gene) {
		return geneService.update(gene);
	}

	public List<GeneDTO> getGene(String primaryId, String symbol, String soTermId, String taxonId, String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(primaryId != null) { map.put("primaryId", primaryId); }
		if(symbol != null) { map.put("symbol", symbol); }
		if(soTermId != null) { map.put("soTermId", soTermId); }
		if(taxonId != null) { map.put("taxonId", taxonId); }
		if(name != null) { map.put("name", name); }
		System.out.println("Input Map: " + map);
		List<GeneDTO> models = geneService.get(map);
		return models;
	}

	public GeneDTO deleteGene(String api_access_token, Long id) {
		return geneService.delete(id);
	}

}
