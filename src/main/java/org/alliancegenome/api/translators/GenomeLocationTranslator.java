package org.alliancegenome.api.translators;

import org.alliancegenome.api.domainmodels.GenomeLocation;
import org.alliancegenome.api.dto.GenomeLocationDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class GenomeLocationTranslator extends EntityDomainTranslator<GenomeLocation, GenomeLocationDTO> {

	@Override
	protected GenomeLocation domainToEntity(GenomeLocationDTO entity) {
		GenomeLocation gl = new GenomeLocation();
		gl.assembly = entity.assembly;
		gl.startPosition = entity.startPosition;
		gl.endPosition = entity.endPosition;
		gl.chromosome = entity.chromosome;
		gl.strand = entity.strand;
		return gl;
	}

	@Override
	protected GenomeLocationDTO entityToDomain(GenomeLocation domain) {
		GenomeLocationDTO gl = new GenomeLocationDTO();
		gl.assembly = domain.assembly;
		gl.startPosition = domain.startPosition;
		gl.endPosition = domain.endPosition;
		gl.chromosome = domain.chromosome;
		gl.strand = domain.strand;
		return gl;
	}

}
