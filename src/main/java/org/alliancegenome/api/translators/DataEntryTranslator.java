package org.alliancegenome.api.translators;

import org.alliancegenome.api.domainmodels.DataEntry;
import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class DataEntryTranslator extends EntityDomainTranslator<DataEntry, DataEntryDTO> {

	@Override
	protected DataEntryDTO entityToDomain(DataEntry entity) {
		DataEntryDTO domain = new DataEntryDTO();
		
		domain.id = entity.id;
		domain.primaryId = entity.primaryId;
		domain.secondaryId = entity.secondaryId;
		domain.tertiaryId = entity.tertiaryId;
		domain.data = entity.data;
		
		return domain;
	}

	@Override
	protected DataEntry domainToEntity(DataEntryDTO domain) {
		DataEntry entity = new DataEntry();
		
		entity.id = domain.id;
		entity.primaryId = domain.primaryId;
		entity.secondaryId = domain.secondaryId;
		entity.tertiaryId = domain.tertiaryId;
		entity.data = domain.data;

		return entity;
	}

}
