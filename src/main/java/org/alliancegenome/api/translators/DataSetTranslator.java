package org.alliancegenome.api.translators;

import org.alliancegenome.api.domainmodels.DataSet;
import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class DataSetTranslator extends EntityDomainTranslator<DataSet, DataSetDTO> {

	@Override
	protected DataSetDTO entityToDomain(DataSet entity) {
		DataSetDTO domain = new DataSetDTO();
		domain.id = entity.id;
		domain.injectdate = entity.injectdate;
		domain.metaData = entity.metaData;
		domain.name = entity.name;
		return domain;
	}

	@Override
	protected DataSet domainToEntity(DataSetDTO domain) {
		DataSet entity = new DataSet();
		
		entity.id = domain.id;
		entity.injectdate = domain.injectdate;
		entity.metaData = domain.metaData;
		entity.name = domain.name;

		return entity;
	}

}
