package org.alliancegenome.api.translators;

import org.alliancegenome.api.domainmodels.CrossReference;
import org.alliancegenome.api.dto.CrossReferenceDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class CrossReferenceTranslator extends EntityDomainTranslator<CrossReference, CrossReferenceDTO> {

	@Override
	protected CrossReferenceDTO entityToDomain(CrossReference entity) {
		CrossReferenceDTO domain = new CrossReferenceDTO();
		domain.id = entity.id;
		domain.dataProvider = entity.dataProvider;
		return domain;
	}

	@Override
	protected CrossReference domainToEntity(CrossReferenceDTO domain) {
		CrossReference entity = new CrossReference();
		entity.id = domain.id;
		entity.dataProvider = domain.dataProvider;
		return entity;
	}

}
