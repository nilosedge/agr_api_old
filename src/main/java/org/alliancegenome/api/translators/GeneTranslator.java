package org.alliancegenome.api.translators;

import java.util.ArrayList;
import java.util.HashSet;

import org.alliancegenome.api.domainmodels.CrossReference;
import org.alliancegenome.api.domainmodels.Gene;
import org.alliancegenome.api.domainmodels.GenomeLocation;
import org.alliancegenome.api.domainmodels.SecondaryId;
import org.alliancegenome.api.domainmodels.Synonym;
import org.alliancegenome.api.dto.CrossReferenceDTO;
import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.api.dto.GenomeLocationDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class GeneTranslator extends EntityDomainTranslator<Gene, GeneDTO> {

	private CrossReferenceTranslator crTranslator = new CrossReferenceTranslator();
	private GenomeLocationTranslator glTranslator = new GenomeLocationTranslator();
	
	@Override
	protected Gene domainToEntity(GeneDTO domain) {
		Gene gene = new Gene();
		
		gene.primaryId = domain.primaryId;
		gene.name = domain.name;
		gene.taxonId = domain.taxonId;
		gene.symbol = domain.symbol;
		gene.geneSynopsis = domain.geneSynopsis;
		gene.geneSynopsisUrl = domain.geneSynopsisUrl;
		gene.geneLiteratureUrl = domain.geneLiteratureUrl;
		gene.soTermId = domain.soTermId;
		
		gene.crossReferences = new HashSet<CrossReference>();
		for(CrossReferenceDTO cr: domain.crossReferences) {
			CrossReference crt = crTranslator.translate(cr);
			crt.gene = gene;
			gene.crossReferences.add(crt);
		}
		
		gene.genomeLocations = new HashSet<GenomeLocation>();
		for(GenomeLocationDTO gl: domain.genomeLocations) {
			GenomeLocation glt = glTranslator.translate(gl);
			glt.gene = gene;
			gene.genomeLocations.add(glt);
		}

		gene.synonyms = new HashSet<Synonym>();
		for(String synonym: domain.synonyms) {
			Synonym s = new Synonym();
			s.synonym = synonym;
			s.gene = gene;
			gene.synonyms.add(s);
		}
		
		gene.secondaryIds = new HashSet<SecondaryId>();
		for(String secondaryId: domain.secondaryIds) {
			SecondaryId s = new SecondaryId();
			s.secondaryId = secondaryId;
			s.gene = gene;
			gene.secondaryIds.add(s);
		}
		
		return gene;
	}

	@Override
	protected GeneDTO entityToDomain(Gene entity) {
		GeneDTO gene = new GeneDTO();

		gene.primaryId = entity.primaryId;
		gene.name = entity.name;
		gene.taxonId = entity.taxonId;
		gene.symbol = entity.symbol;
		gene.geneSynopsis = entity.geneSynopsis;
		gene.geneSynopsisUrl = entity.geneSynopsisUrl;
		gene.geneLiteratureUrl = entity.geneLiteratureUrl;
		gene.soTermId = entity.soTermId;
		
		gene.crossReferences = new ArrayList<CrossReferenceDTO>();
		for(CrossReference cr: entity.crossReferences) {
			gene.crossReferences.add(crTranslator.translate(cr));
		}
		
		gene.genomeLocations = new ArrayList<GenomeLocationDTO>();
		for(GenomeLocation gl: entity.genomeLocations) {
			gene.genomeLocations.add(glTranslator.translate(gl));
		}
		
		gene.synonyms = new ArrayList<String>();
		for(Synonym synonym: entity.synonyms) {
			gene.synonyms.add(synonym.synonym);
		}
		
		gene.secondaryIds = new ArrayList<String>();
		for(SecondaryId secondaryId: entity.secondaryIds) {
			gene.secondaryIds.add(secondaryId.secondaryId);
		}
		
		return gene;
	}

}
