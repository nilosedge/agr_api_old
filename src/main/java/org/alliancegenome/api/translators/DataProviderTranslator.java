package org.alliancegenome.api.translators;

import org.alliancegenome.api.domainmodels.DataProvider;
import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.interfaces.EntityDomainTranslator;

public class DataProviderTranslator extends EntityDomainTranslator<DataProvider, DataProviderDTO> {

	@Override
	protected DataProviderDTO entityToDomain(DataProvider entity) {
		DataProviderDTO domain = new DataProviderDTO();
		
		domain.id = entity.id;
		domain.metaData = entity.metaData;
		domain.name = entity.name;
		
		return domain;
	}

	@Override
	protected DataProvider domainToEntity(DataProviderDTO domain) {
		DataProvider entity = new DataProvider();
		
		entity.id = domain.id;
		entity.metaData = domain.metaData;
		entity.name = domain.name;
		
		return entity;
	}

}
