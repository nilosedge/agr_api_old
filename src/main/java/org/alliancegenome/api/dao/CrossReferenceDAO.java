package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.CrossReference;

@Default
public class CrossReferenceDAO extends PostgresSQLDAO<CrossReference> {

	public CrossReferenceDAO() {
		clazz = CrossReference.class;
	}

}
