package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.DataSet;

@Default
public class DataSetDAO extends PostgresSQLDAO<DataSet> {

	public DataSetDAO() {
		clazz = DataSet.class;
	}

}
