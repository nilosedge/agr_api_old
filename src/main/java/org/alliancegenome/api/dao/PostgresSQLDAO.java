package org.alliancegenome.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.alliancegenome.api.domainmodels.BaseDomainModel;
import org.apache.log4j.Logger;

@Default
public class PostgresSQLDAO<T extends BaseDomainModel> {

	protected Class<T> clazz;

	@PersistenceContext
	private EntityManager entityManager;
	
	private Logger log = Logger.getLogger(getClass());

	protected void setClazz(Class<T> clazz){
		this.clazz = clazz;
	}

	public T add(T model) {
		//EntityTransaction t = entityManager.getTransaction();
		//t.begin();
		entityManager.persist(model);
		//t.commit();
		return model;
	}

	public List<T> addBatch(List<T> models) {
		log.info("Adding " + models.size() + " Models");
		//EntityTransaction t = entityManager.getTransaction();
		//t.begin();
		for(T model: models) {
			entityManager.persist(model);
		}

		//t.commit();
		return models;
	}

	public T update(T model) {
		entityManager.merge(model);
		return model;
	}

	public List<T> get(HashMap<String, Object> params) {
		log.info("Lookup: " + params);
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz);
		Root<T> root = query.from(clazz);

		List<Predicate> restrictions = new ArrayList<Predicate>();
		
		for(String key: params.keySet()) {
			restrictions.add(builder.equal(root.get(key), params.get(key)));
		}

		query.where(builder.and(restrictions.toArray(new Predicate[0])));

		return entityManager.createQuery(query).getResultList();
	}

	public T delete(T model) {
		entityManager.remove(model);
		return model;
	}

}
