package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.DataEntry;

@Default
public class DataEntryDAO extends PostgresSQLDAO<DataEntry> {

	public DataEntryDAO() {
		clazz = DataEntry.class;
	}

}
