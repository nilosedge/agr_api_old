package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.GenomeLocation;

@Default
public class GenomeLocationDAO extends PostgresSQLDAO<GenomeLocation> {

	public GenomeLocationDAO() {
		clazz = GenomeLocation.class;
	}

}