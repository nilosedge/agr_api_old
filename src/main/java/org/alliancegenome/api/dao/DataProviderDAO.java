package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.DataProvider;

@Default
public class DataProviderDAO extends PostgresSQLDAO<DataProvider> {

	public DataProviderDAO() {
		clazz = DataProvider.class;
	}

}
