package org.alliancegenome.api.dao;

import javax.enterprise.inject.Default;

import org.alliancegenome.api.domainmodels.CrossReference;
import org.alliancegenome.api.domainmodels.Gene;
import org.alliancegenome.api.domainmodels.GenomeLocation;
import org.alliancegenome.api.domainmodels.SecondaryId;
import org.alliancegenome.api.domainmodels.Synonym;

@Default
public class GeneDAO extends PostgresSQLDAO<Gene> {

	//private CrossReferenceDAO crDAO;
	//private GenomeLocationDAO glDAO;

	public GeneDAO() {
		clazz = Gene.class;
	}

	public Gene addGene(Gene model) {
		updateCrossReferences(model);
		updateGenomeLocations(model);
		updateSynonyms(model);
		updateSecondaryIds(model);
		return add(model);
	}

	private void updateSecondaryIds(Gene model) {
		for(SecondaryId s: model.secondaryIds) {
			s.gene = model;
		}
	}

	private void updateSynonyms(Gene model) {
		for(Synonym s: model.synonyms) {
			s.gene = model;
		}
	}

	private void updateGenomeLocations(Gene model) {
		for(GenomeLocation glm: model.genomeLocations) {
			//System.out.println(glm);
			glm.gene = model;
			//glDAO.update(glm);
		}
	}

	private void updateCrossReferences(Gene model) {
		for(CrossReference crm: model.crossReferences) {
			//System.out.println(crm);
			crm.gene = model;
			//crDAO.update(crm);
		}
	}

	public Gene updateGene(Gene model) {
		updateCrossReferences(model);
		updateGenomeLocations(model);
		return update(model);
	}

}
