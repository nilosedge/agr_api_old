package org.alliancegenome.api.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.alliancegenome.api.dao.DataProviderDAO;
import org.alliancegenome.api.dao.DataSetDAO;
import org.alliancegenome.api.domainmodels.DataProvider;
import org.alliancegenome.api.domainmodels.DataSet;
import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.api.interfaces.BaseServiceInterface;
import org.alliancegenome.api.translators.DataSetTranslator;

@Default
public class DataSetService implements BaseServiceInterface<DataSetDTO> {
	
	@Inject
	private DataProviderDAO dataProviderDAO;
	@Inject
	private DataSetDAO dataSetDAO;
	
	//private Logger log = Logger.getLogger(getClass());
	
	private DataSetTranslator translator = new DataSetTranslator();

	@Transactional
	public DataSetDTO add(DataSetDTO model) {
		model.injectdate = new Date();
		return translator.translate(dataSetDAO.add(translator.translate(model)));
	}
	
	@Transactional
	public DataSetDTO add(Long dataProviderId, DataSetDTO model) {

		DataSet entity = translator.translate(model);
		
		if(dataProviderId != null) {
			HashMap<String, Object> map = new HashMap<>();
			map.put("id", dataProviderId);
			List<DataProvider> dps = dataProviderDAO.get(map);
			if(dps != null && dps.size() > 0) {
				entity.dataProvider = dps.get(0);
			}
		}
		
		return translator.translate(dataSetDAO.add(entity));

	}

	@Transactional
	public DataSetDTO update(DataSetDTO model) {
		return translator.translate(dataSetDAO.update(translator.translate(model)));
	}

	@Transactional(value=TxType.NEVER)
	public List<DataSetDTO> get(HashMap<String, Object> searchFields) {
		return translator.translateEntities(dataSetDAO.get(searchFields));
	}

	@Transactional
	public DataSetDTO delete(Object key) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(key != null) { map.put("id", key); }
		DataSet model = dataSetDAO.get(map).get(0);
		
		return translator.translate(dataSetDAO.delete(model));
	}


}
