package org.alliancegenome.api.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.alliancegenome.api.dao.GeneDAO;
import org.alliancegenome.api.domainmodels.Gene;
import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.api.interfaces.BaseServiceInterface;
import org.alliancegenome.api.translators.GeneTranslator;

@Default
public class GeneService implements BaseServiceInterface<GeneDTO> {

	@Inject
	private GeneDAO geneDAO;
	//private Logger log = Logger.getLogger(getClass());
	private GeneTranslator translator = new GeneTranslator();

	@Transactional
	public GeneDTO add(GeneDTO model) {
		if(model.isValid()) {
			return translator.translate(geneDAO.addGene(translator.translate(model)));
		} else {
			return null;
		}
	}

	@Transactional
	public List<GeneDTO> addBatch(List<GeneDTO> models) {
		
		List<Gene> genes = new ArrayList<>();
		for(GeneDTO model: models) {
			if(model.isValid()) {
				genes.add(translator.translate(model));
			}
		}
		
		genes = geneDAO.addBatch(genes);
		
		List<GeneDTO> ret = new ArrayList<>();
		for(Gene gene: genes) {
			ret.add(translator.translate(gene));
		}
		
		return ret;
	}

	@Transactional
	public GeneDTO update(GeneDTO model) {
		if(model.isValid()) {
			return translator.translate(geneDAO.updateGene(translator.translate(model)));
		} else {
			return null;
		}
	}

	@Transactional(value=TxType.NEVER)
	public List<GeneDTO> get(HashMap<String, Object> searchFields) {
		List<GeneDTO> list = new ArrayList<GeneDTO>();
		for(Gene g: geneDAO.get(searchFields)) {
			list.add(translator.translate(g));
		}
		return list;
	}

	@Transactional
	public GeneDTO delete(Object key) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(key != null) { map.put("primaryId", key); }
		Gene model = geneDAO.get(map).get(0);
		return translator.translate(geneDAO.delete(model));
	}



}
