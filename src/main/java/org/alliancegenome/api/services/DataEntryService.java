package org.alliancegenome.api.services;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.alliancegenome.api.dao.DataEntryDAO;
import org.alliancegenome.api.dao.DataSetDAO;
import org.alliancegenome.api.domainmodels.DataEntry;
import org.alliancegenome.api.domainmodels.DataSet;
import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.interfaces.BaseServiceInterface;
import org.alliancegenome.api.translators.DataEntryTranslator;

@Default
public class DataEntryService implements BaseServiceInterface<DataEntryDTO> {
	
	@Inject
	private DataSetDAO dataSetDAO;
	@Inject
	private DataEntryDAO dataEntryDAO;
	
	//private Logger log = Logger.getLogger(getClass());
	
	private DataEntryTranslator tranlator = new DataEntryTranslator();

	public DataEntryDTO add(DataEntryDTO model) {
		return add(model, null);
	}

	@Transactional
	public DataEntryDTO add(DataEntryDTO model, Long dataSetId) {
		DataEntry entity = tranlator.translate(model);
		
		if(dataSetId != null) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("id", dataSetId);
			List<DataSet> dss = dataSetDAO.get(map);
			if(dss != null && dss.size() > 0) {
				entity.dataSet = dss.get(0);
			}
		}
		
		return tranlator.translate(dataEntryDAO.add(entity));
	}

	public List<DataEntryDTO> addBatch(List<DataEntryDTO> models) {
		return addBatch(models, null);
	}

	@Transactional
	public List<DataEntryDTO> addBatch(List<DataEntryDTO> models, Long dataSetId) {
		List<DataEntry> saveList = tranlator.translateDomains(models);
		if(dataSetId != null) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("id", dataSetId);
			DataSet dataSet = dataSetDAO.get(map).get(0);

			if(dataSet != null) {
				for(DataEntry entry: saveList) {
					entry.dataSet = dataSet;
				}
			}
		}
		
		return tranlator.translateEntities(dataEntryDAO.addBatch(saveList));
	}

	@Transactional
	public DataEntryDTO update(DataEntryDTO model) {
		return tranlator.translate(dataEntryDAO.update(tranlator.translate(model)));
	}

	@Transactional
	public List<DataEntryDTO> get(HashMap<String, Object> searchFields) {
		return tranlator.translateEntities(dataEntryDAO.get(searchFields));
	}

	@Transactional
	public DataEntryDTO delete(Object key) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(key != null) { map.put("id", key); }
		DataEntry model = dataEntryDAO.get(map).get(0);

		return tranlator.translate(dataEntryDAO.delete(model));
	}

}
