package org.alliancegenome.api.services;

import java.util.HashMap;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.alliancegenome.api.dao.DataProviderDAO;
import org.alliancegenome.api.domainmodels.DataProvider;
import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.interfaces.BaseServiceInterface;
import org.alliancegenome.api.translators.DataProviderTranslator;

@Default
public class DataProviderService implements BaseServiceInterface<DataProviderDTO> {
	
	@Inject
	private DataProviderDAO dataProviderDAO;
	
	//private Logger log = Logger.getLogger(getClass());
	
	private DataProviderTranslator translator = new DataProviderTranslator();

	@Transactional
	public DataProviderDTO add(DataProviderDTO model) {
		return translator.translate(dataProviderDAO.add(translator.translate(model)));
	}

	@Transactional
	public DataProviderDTO update(DataProviderDTO model) {
		return translator.translate(dataProviderDAO.update(translator.translate(model)));
	}

	@Transactional(value=TxType.NEVER)
	public List<DataProviderDTO> get(HashMap<String, Object> searchFields) {
		return translator.translateEntities(dataProviderDAO.get(searchFields));
	}

	@Transactional
	public DataProviderDTO delete(Object key) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(key != null) { map.put("id", key); }
		DataProvider model = dataProviderDAO.get(map).get(0);
		return translator.translate(dataProviderDAO.delete(model));
	}

	
}
