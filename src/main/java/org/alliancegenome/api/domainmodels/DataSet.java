package org.alliancegenome.api.domainmodels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="data_set")
public class DataSet extends BaseDomainModel {
	
	@Id
	@GeneratedValue
	public Long id;
	public String name;
	@Column(columnDefinition="TEXT")
	public String metaData;
	public Date injectdate;
	
	@JsonIgnore
	@ApiModelProperty(required=false)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dataSet")
	public List<DataEntry> dataEntries = new ArrayList<DataEntry>();
	
	@JsonIgnore
	@ApiModelProperty(required=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public DataProvider dataProvider;
	
	public String toString() {
		return "{ id: " + id + ", name: " + name + "}";
	}
	
	@Override
	public void Accept(ModelVisitorInterface pi) {
		pi.Visit(this);
	}
}
