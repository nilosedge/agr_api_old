package org.alliancegenome.api.domainmodels;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;
import org.alliancegenome.api.util.GeneVisitor;

@Entity
@Table(name="gene")
public class Gene extends BaseDomainModel {
	
	@Id
	@GeneratedValue
	public Long key;
	
	public String primaryId;
	public String name;
	public String taxonId;
	public String symbol;
	@Column(columnDefinition="TEXT")
	public String geneSynopsis;
	public String geneSynopsisUrl;
	public String geneLiteratureUrl;
	public String soTermId;
	

	@OneToMany(mappedBy = "gene", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	public Set<Synonym> synonyms = new HashSet<Synonym>();
	
	@OneToMany(mappedBy = "gene", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	public Set<CrossReference> crossReferences = new HashSet<CrossReference>();
	
	@OneToMany(mappedBy = "gene", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	public Set<GenomeLocation> genomeLocations = new HashSet<GenomeLocation>();
	
	@OneToMany(mappedBy = "gene", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	public Set<SecondaryId> secondaryIds = new HashSet<SecondaryId>();

	@Override
	public void Accept(ModelVisitorInterface pi) {
		pi.Visit(this);
	}
	
	public String toString() {
		GeneVisitor v = new GeneVisitor();
		Accept(v);
		return v.generateOutput();
	}
}
