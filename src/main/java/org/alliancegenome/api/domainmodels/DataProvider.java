package org.alliancegenome.api.domainmodels;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;

@Entity
@Table(name="data_provider", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"}) })
public class DataProvider extends BaseDomainModel {

	@Id
	@GeneratedValue
	public Long id;
	public String name;
	@Column(columnDefinition="TEXT")
	public String metaData;

	@OneToMany(mappedBy = "dataProvider", fetch = FetchType.EAGER)
	public Set<DataSet> dataSets = new HashSet<DataSet>();
	
	public String toString() {
		return "{ id: " + id + ", name: " + name + "}";
	}
	
	@Override
	public void Accept(ModelVisitorInterface pi) {
		pi.Visit(this);
	}
}
