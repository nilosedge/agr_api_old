package org.alliancegenome.api.domainmodels;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="secondary_id")
public class SecondaryId extends BaseDomainModel {

	@Id
	@GeneratedValue
	public Long key;
	
	public String secondaryId;
	
	@ApiModelProperty(required=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Gene gene;
	
	@Override
	public void Accept(ModelVisitorInterface pi) {
		pi.Visit(this);
	}
}
