package org.alliancegenome.api.domainmodels;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;

public abstract class BaseDomainModel {

	public abstract void Accept(ModelVisitorInterface pi);
}
