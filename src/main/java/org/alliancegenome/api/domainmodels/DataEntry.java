package org.alliancegenome.api.domainmodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.alliancegenome.api.interfaces.ModelVisitorInterface;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="data_entry")
public class DataEntry extends BaseDomainModel {
	
	@Id
	@GeneratedValue
	public Long id;
	
	// Indexed fields to lookup this entity
	public String primaryId;
	public String secondaryId;
	public String tertiaryId;
	
	@Column(columnDefinition="TEXT")
	public String data;
	
	@JsonIgnore
	@ApiModelProperty(required=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public DataSet dataSet;
	
	public String toString() {
		return "{ primaryId: " + primaryId + ", data: " + data + "}";
	}
	
	@Override
	public void Accept(ModelVisitorInterface pi) {
		pi.Visit(this);
	}
}
