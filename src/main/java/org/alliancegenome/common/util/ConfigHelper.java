package org.alliancegenome.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class ConfigHelper {

	private static Logger log;
	private static Properties configProperties = new Properties();

	private static HashMap<String, String> defaults = new HashMap<String, String>();
	private static HashMap<String, String> config = new HashMap<String, String>();

	public static void init() {
		BasicConfigurator.configure();
		//PropertyConfigurator.configure(ConfigHelper.class.getResource("log4j.properties"));
		log = Logger.getLogger(ConfigHelper.class);

		defaults.put("API_URL", "http://localhost:8080/api");
		defaults.put("API_ACCESS_TOKEN", "api_password");
		defaults.put("DEBUG", "false");

		for(String key: defaults.keySet()) {
			if(!config.containsKey(key)) config.put(key, null);
			if(config.get(key) == null) config.put(key, loadSystemProperty(key));
			if(config.get(key) == null) config.put(key, loadConfigProperty(key));
			if(config.get(key) == null) config.put(key, loadSystemENVProperty(key));
			if(config.get(key) == null) config.put(key, loadDefaultProperty(key));
		}
		printProperties();
	}

	private static String loadSystemProperty(String key) {
		String ret = System.getProperty(key);
		if(ret != null) log.info("Found: -D " + key + "=" + ret);
		return ret;
	}

	private static String loadConfigProperty(String key) {
		if(configProperties.size() == 0) {
			InputStream in = ConfigHelper.class.getClassLoader().getResourceAsStream("config.properties");
			if(in == null) {
				log.info("No config.properties file, other config options will be used for: " + key);
			} else {
				try {
					configProperties.load(in);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		String ret = configProperties.getProperty(key);
		if(ret != null) log.info("Config File Property: " + key + "=" + ret);
		return ret;
	}

	private static String loadSystemENVProperty(String key) {
		String ret = System.getenv(key);
		if(ret != null) log.info("Found Enviroment ENV[" + key + "]=" + ret);
		return ret;
	}

	private static String loadDefaultProperty(String key) {
		String ret = defaults.get(key);
		if(ret != null) log.info("Setting default: " + key + "=" + ret);
		return ret;
	}

	public static String getApiUrl() {
		return config.get("API_URL");
	}
	public static String getApiAccessToken() {
		return config.get("API_ACCESS_TOKEN");
	}
	public static boolean getDebug() {
		return Boolean.parseBoolean(config.get("DEBUG"));
	}


	public static void printProperties() {
		log.info("Running with Properties:");
		for(String key: defaults.keySet()) {
			log.info("\t" + key + ": " + config.get(key));
		}
	}
	
}
