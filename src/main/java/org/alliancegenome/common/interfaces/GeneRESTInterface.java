package org.alliancegenome.common.interfaces;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.alliancegenome.api.dto.GeneDTO;

import io.swagger.annotations.Api;

@Api(value = "/gene")
@Path("/gene")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface GeneRESTInterface {

	@POST
	public GeneDTO createGene(@HeaderParam("api_access_token") String api_access_token, GeneDTO gene);
	
	@POST
	@Path("/batch")
	public List<GeneDTO> createGeneBatch(@HeaderParam("api_access_token") String api_access_token, List<GeneDTO> genes);

	@PUT
	public GeneDTO updateGene(@HeaderParam("api_access_token") String api_access_token, GeneDTO gene);

	@GET
	public List<GeneDTO> getGene(
			@QueryParam("primaryId") String primaryId,
			@QueryParam("symbol") String symbol,
			@QueryParam("soTermId") String soTermId,
			@QueryParam("taxonId") String taxonId,
			@QueryParam("name") String name);

	@DELETE
	@Path("/{id}")
	public GeneDTO deleteGene(@HeaderParam("api_access_token") String api_access_token, @PathParam("id") Long id);
	
}
