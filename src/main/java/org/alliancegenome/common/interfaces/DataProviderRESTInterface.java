package org.alliancegenome.common.interfaces;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.alliancegenome.api.dto.DataProviderDTO;

import io.swagger.annotations.Api;

@Api(value = "/dataprovider")
@Path("/dataprovider")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface DataProviderRESTInterface {

	@POST
	public DataProviderDTO createDataProvider(@HeaderParam("api_access_token") String api_access_token, DataProviderDTO dataProvider);

	@PUT
	public DataProviderDTO updateDataProvider(@HeaderParam("api_access_token") String api_access_token, DataProviderDTO dataProvider);

	@GET
	public List<DataProviderDTO> getDataProvider(@QueryParam("id") Long id, @QueryParam("name") String name);

	@DELETE
	@Path("/{id}")
	public DataProviderDTO deleteDataProvider(@HeaderParam("api_access_token") String api_access_token, @PathParam("id") Long id);

}
