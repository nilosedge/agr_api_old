package org.alliancegenome.common.interfaces;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.alliancegenome.api.dto.DataEntryDTO;

import io.swagger.annotations.Api;

@Api(value = "/dataentry")
@Path("/dataentry")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface DataEntryRESTInterface {

	@POST
	@Path("/{dataSetId}")
	
	public DataEntryDTO createDataEntry(@HeaderParam("api_access_token") String api_access_token, @PathParam("dataSetId") Long dataSetId, DataEntryDTO dataEntry);

	@POST
	@Path("/batch/{dataSetId}")
	public List<DataEntryDTO> createDataEntryBatch(@HeaderParam("api_access_token") String api_access_token, @PathParam("dataSetId") Long dataSetId, List<DataEntryDTO> dataEntry);

	@PUT
	public DataEntryDTO updateDataEntry(@HeaderParam("api_access_token") String api_access_token, DataEntryDTO dataEntry);

	@GET
	public List<DataEntryDTO> getDataEntry(@QueryParam("id") Long id);

	@DELETE
	@Path("/{id}")
	public DataEntryDTO deleteDataEntry(@HeaderParam("api_access_token") String api_access_token, @PathParam("id") Long id);
	
}
