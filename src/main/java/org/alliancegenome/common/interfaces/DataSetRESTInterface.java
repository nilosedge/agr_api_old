package org.alliancegenome.common.interfaces;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.alliancegenome.api.dto.DataSetDTO;

import io.swagger.annotations.Api;

@Api(value = "/dataset")
@Path("/dataset")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface DataSetRESTInterface {

	@POST
	@Path("/{dataProviderId}")
	public DataSetDTO createDataSet(@HeaderParam("api_access_token") String api_access_token, @PathParam("dataProviderId") Long dataProviderId, DataSetDTO dataSet);

	@PUT
	public DataSetDTO updateDataSet(@HeaderParam("api_access_token") String api_access_token, DataSetDTO dataSet);

	@GET
	public List<DataSetDTO> getDataSet(@QueryParam("id") Long id, @QueryParam("name") String name);

	@DELETE
	@Path("/{id}")
	public DataSetDTO deleteDataSet(@HeaderParam("api_access_token") String api_access_token, @PathParam("id") Long id);

}
