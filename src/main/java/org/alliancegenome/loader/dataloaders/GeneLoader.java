package org.alliancegenome.loader.dataloaders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.common.util.ConfigHelper;
import org.alliancegenome.loader.api.DataEntryControllerRESTAPI;
import org.alliancegenome.loader.api.DataProviderControllerRESTAPI;
import org.alliancegenome.loader.api.DataSetControllerRESTAPI;
import org.alliancegenome.loader.files.S3File;
import org.alliancegenome.loader.files.TARFile;
import org.alliancegenome.loader.files.TXTFile;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class GeneLoader extends Loader {
	
	private String outputFileName = null;
	private List<DataEntryDTO> entries = new ArrayList<DataEntryDTO>();
	private DataProviderControllerRESTAPI dataProviderAPI = new DataProviderControllerRESTAPI(ConfigHelper.getApiAccessToken(), ConfigHelper.getApiUrl());
	private DataSetControllerRESTAPI dataSetAPI = new DataSetControllerRESTAPI(ConfigHelper.getApiAccessToken(), ConfigHelper.getApiUrl());
	private DataEntryControllerRESTAPI dataEntryAPI = new DataEntryControllerRESTAPI(ConfigHelper.getApiAccessToken(), ConfigHelper.getApiUrl());
	
	private Logger log = Logger.getLogger(getClass());
	
	public GeneLoader(String dataProvider, String dataSetName, String dataFileName, String outputFileName) {
		this.dataProvider = dataProvider;
		this.dataSetName = dataSetName;
		this.dataFileName = dataFileName;
		this.outputFileName = outputFileName;
	}

	public void loadData() throws FileNotFoundException, IOException {
		S3File s3 = new S3File(s3RootPath, dataFileName);
		s3.download();
		
		TARFile tar = new TARFile(dataFileName, outputFileName);
		tar.extract_all();
		
		TXTFile txt = new TXTFile(outputFileName);
		BufferedReader reader = txt.getBufferedReader();
		
		String jsonString = IOUtils.toString(reader);
		
		JSONObject jo = new JSONObject(jsonString);
		
		JSONArray geneObjects = jo.getJSONArray("data");
		
		for(int i = 0; i < geneObjects.length(); i++) {
			JSONObject gene = geneObjects.getJSONObject(i);
			DataEntryDTO dto = new DataEntryDTO();

			if(!gene.isNull("primaryId")) {
				dto.primaryId = (String)gene.get("primaryId");
			}

			dto.data = gene.toString();
			entries.add(dto);
		}

	}
	
	public void importData() {
		DataProviderDTO dp = null;
		List<DataProviderDTO> dps = dataProviderAPI.getDataProvider(null, dataProvider);
		log.info(dps);
		if(dps == null || dps.size() == 0) {
			log.info("Create new DP: " + dataProvider);
		dp = dataProviderAPI.createDataProvider(new DataProviderDTO(dataProvider));
		} else {
			dp = dps.get(0);
		}
		
		log.info("Data Provider: " + dp);
		
		DataSetDTO ds = null;
		//List<DataSetDTO> dss = dataSetAPI.getDataSet(null, dataSetName);
		//if(dss == null || dss.size() == 0) {
		ds = dataSetAPI.createDataSet(dp.id, new DataSetDTO(dataSetName));
		//} else {
		//	ds = dss.get(0);
		//}
		
		log.info("Data Set: " + ds);
		
		
		// Batched
		List<DataEntryDTO> temp = new ArrayList<DataEntryDTO>();
		int count = 0;
		for(DataEntryDTO object: entries) {
			DataEntryDTO ent = object;
			temp.add(ent);
			if(++count % 10000 == 0) {
				dataEntryAPI.createDataEntryBatch(ds.id, temp);
				temp.clear();
			}
		}
		if(temp.size() > 0) {
			dataEntryAPI.createDataEntryBatch(ds.id, temp);
			temp.clear();
		}
		
		// Non Batched
//		int count = 0;
//		for(DataEntryDTO ent: entries) {
//			count++;
//			dataEntryAPI.createDataEntry(ds.id, ent);
//		}
		
		log.info("Loaded: " + count + " models into the database");

	}

	
	
}
