package org.alliancegenome.loader.dataloaders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.common.util.ConfigHelper;
import org.alliancegenome.loader.api.GeneControllerRESTAPI;
import org.alliancegenome.loader.files.S3File;
import org.alliancegenome.loader.files.TARFile;
import org.alliancegenome.loader.files.TXTFile;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GeneLoader2 extends Loader {

	private String outputFileName = null;
	private List<GeneDTO> entries = new ArrayList<GeneDTO>();
	private ObjectMapper mapper = new ObjectMapper();
	private GeneControllerRESTAPI api = new GeneControllerRESTAPI(ConfigHelper.getApiAccessToken(), ConfigHelper.getApiUrl());
	
	public GeneLoader2(String dataProvider, String dataSetName, String dataFileName, String outputFileName) {
		this.dataProvider = dataProvider;
		this.dataSetName = dataSetName;
		this.dataFileName = dataFileName;
		this.outputFileName = outputFileName;
	}

	public void loadData() throws FileNotFoundException, IOException {
		S3File s3 = new S3File(s3RootPath, dataFileName);
		s3.download();

		TARFile tar = new TARFile(dataFileName, outputFileName);
		tar.extract_all();

		TXTFile txt = new TXTFile(outputFileName);
		BufferedReader reader = txt.getBufferedReader();

		String jsonString = IOUtils.toString(reader);

		JSONObject o = new JSONObject(jsonString);

		JSONArray geneObjects = o.getJSONArray("data");

		for(int i = 0; i < geneObjects.length(); i++) {
			JSONObject gene = geneObjects.getJSONObject(i);
			GeneDTO geneDto = mapper.readValue(gene.toString(), GeneDTO.class);
			entries.add(geneDto);
		}

	}

	public void importData() {


		boolean batched = true;
		int count = 0;
		if(batched) {

			List<GeneDTO> temp = new ArrayList<GeneDTO>();

			for(GeneDTO ent: entries) {
				temp.add(ent);
				if(++count % 100000 == 0) {
					api.createGeneBatch(temp);
					temp.clear();
				}
			}
			if(temp.size() > 0) {
				api.createGeneBatch(temp);
				temp.clear();
			}
		} else {

			for(GeneDTO ent: entries) {
				count++;
				api.createGene(ent);
			}
		}

		System.out.println("Loaded: " + count + " models into the database");

	}

}
