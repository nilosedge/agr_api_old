package org.alliancegenome.loader.dataloaders;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import org.apache.log4j.Logger;

public abstract class Loader {

	protected String dataProvider;
	protected String dataSetName;
	protected String dataFileName;
	protected String s3RootPath = "mod-datadumps";
	protected Date start;
	protected Date end;
	
	private Logger log = Logger.getLogger(getClass());
	
	public abstract void loadData() throws FileNotFoundException, IOException;
	public abstract void importData();
	
	public void start() {
		start = new Date();
	}

	public void finish() {
		end = new Date();
		log.info("Loading Took: " + (end.getTime() - start.getTime()) + "ms");
	}
}
