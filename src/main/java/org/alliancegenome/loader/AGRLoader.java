package org.alliancegenome.loader;

import java.io.IOException;
import java.util.HashMap;

import org.alliancegenome.common.util.ConfigHelper;
import org.alliancegenome.loader.dataloaders.GeneLoader;
import org.alliancegenome.loader.dataloaders.Loader;

public class AGRLoader {

	public static void main(String[] args) {
		new AGRLoader();
	}
	
	public AGRLoader() {
		
		System.out.println("Loader Running");
		
		ConfigHelper.init();
		
		HashMap<String, Loader> loaders = new HashMap<String, Loader>();
		
		//loaders.put("Go Loader", new GoLoader());
		loaders.put("Gene Loader MGI", new GeneLoader("MGI", "BGI", "MGI_0.3.0_1.tar.gz", "MGI_0.3_basicGeneInformation.json"));
		loaders.put("Gene Loader ZFIN", new GeneLoader("ZFIN", "BGI", "ZFIN_0.3.0_6.tar.gz", "ZFIN_0.3.0_BGI.json"));
		loaders.put("Gene Loader Wormbase", new GeneLoader("WB", "BGI", "WB_0.3.0_2.tar.gz", "WB_0.3_basicgeneinformation.json"));
		loaders.put("Gene Loader Human", new GeneLoader("RGD", "BGI", "RGD_0.3_1.tar.gz", "agr/RGD_0.3_basicGeneInformation.9606.json"));
		
		//loaders.put("Gene Loader MGI2", new GeneLoader2("MGI", "BGI", "MGI_0.3.0_1.tar.gz", "MGI_0.3_basicGeneInformation.json"));
		//loaders.put("Gene Loader ZFIN2", new GeneLoader2("ZFIN", "BGI", "ZFIN_0.3.0_6.tar.gz", "ZFIN_0.3.0_BGI.json"));
		//loaders.put("Gene Loader Wormbase2", new GeneLoader2("WB", "BGI", "WB_0.3.0_2.tar.gz", "WB_0.3_basicgeneinformation.json"));
		//loaders.put("Gene Loader Human2", new GeneLoader2("RGD", "BGI", "RGD_0.3_1.tar.gz", "agr/RGD_0.3_basicGeneInformation.9606.json"));
		
		
		for(Loader l: loaders.values()) {
			try {
				l.start();
				l.loadData();
				l.importData();
				l.finish();
			} catch (IOException e) {
				System.out.println("Loader Failed");
				e.printStackTrace();
			}
			
		}

	}
}
