package org.alliancegenome.loader.api;

import java.util.List;

import org.alliancegenome.api.dto.DataProviderDTO;
import org.alliancegenome.common.interfaces.DataProviderRESTInterface;

import si.mazi.rescu.RestProxyFactory;

public class DataProviderControllerRESTAPI {

	private String api_access_token;
	private DataProviderRESTInterface api = null;
	
	public DataProviderControllerRESTAPI(String api_access_token, String baseURL) {
		this.api_access_token = api_access_token;
		api = RestProxyFactory.createProxy(DataProviderRESTInterface.class, baseURL);
	}
	
	public DataProviderDTO createDataProvider(DataProviderDTO dataProvider) {
		try {
			return api.createDataProvider(api_access_token, dataProvider);
		} catch (Exception e) {
			return null;
		}
	}

	public DataProviderDTO updateDataProvider(DataProviderDTO dataProvider) {
		try {
			return api.updateDataProvider(api_access_token, dataProvider);
		} catch (Exception e) {
			return null;
		}
	}

	public List<DataProviderDTO> getDataProvider(Long id, String name) {
		try {
			return api.getDataProvider(id, name);
		} catch (Exception e) {
			return null;
		}
	}

	public DataProviderDTO deleteDataProvider(Long id) {
		try {
			return api.deleteDataProvider(api_access_token, id);
		} catch (Exception e) {
			return null;
		}
	}
}
