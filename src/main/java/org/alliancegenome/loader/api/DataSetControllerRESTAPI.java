package org.alliancegenome.loader.api;

import java.util.List;

import org.alliancegenome.api.dto.DataSetDTO;
import org.alliancegenome.common.interfaces.DataSetRESTInterface;

import si.mazi.rescu.RestProxyFactory;

public class DataSetControllerRESTAPI {

	private String api_access_token;
	private DataSetRESTInterface api = null;
	
	public DataSetControllerRESTAPI(String api_access_token, String baseURL) {
		this.api_access_token = api_access_token;
		api = RestProxyFactory.createProxy(DataSetRESTInterface.class, baseURL);
	}
	
	public DataSetDTO createDataSet(Long dataProviderId, DataSetDTO dataSet) {
		try {
			return api.createDataSet(api_access_token, dataProviderId, dataSet);
		} catch (Exception e) {
			return null;
		}
	}

	public DataSetDTO updateDataSet(DataSetDTO dataSet) {
		try {
			return api.updateDataSet(api_access_token, dataSet);
		} catch (Exception e) {
			return null;
		}
	}

	public List<DataSetDTO> getDataSet(Long id, String name) {
		try {
			return api.getDataSet(id, name);
		} catch (Exception e) {
			return null;
		}
	}

	public DataSetDTO deleteDataSet(Long id) {
		try {
			return api.deleteDataSet(api_access_token, id);
		} catch (Exception e) {
			return null;
		}
	}
	
}
