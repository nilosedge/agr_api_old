package org.alliancegenome.loader.api;

import java.util.List;

import org.alliancegenome.api.dto.DataEntryDTO;
import org.alliancegenome.common.interfaces.DataEntryRESTInterface;

import si.mazi.rescu.RestProxyFactory;

public class DataEntryControllerRESTAPI {

	private String api_access_token;
	private DataEntryRESTInterface api = null;

	public DataEntryControllerRESTAPI(String api_access_token, String baseURL) {
		this.api_access_token = api_access_token;
		api = RestProxyFactory.createProxy(DataEntryRESTInterface.class, baseURL);
	}

	public DataEntryDTO createDataEntry(Long dataSetId, DataEntryDTO dataEntry) {
		try {
			return api.createDataEntry(api_access_token, dataSetId, dataEntry);
		} catch (Exception e) {
			return null;
		}
	}

	public List<DataEntryDTO> createDataEntryBatch(Long dataSetId, List<DataEntryDTO> dataEntry) {
		try {
			return api.createDataEntryBatch(api_access_token, dataSetId, dataEntry);
		} catch (Exception e) {
			return null;
		}
	}

	public DataEntryDTO updateDataEntry(DataEntryDTO dataSet) {
		try {
			return api.updateDataEntry(api_access_token, dataSet);
		} catch (Exception e) {
			return null;
		}
	}

	public List<DataEntryDTO> getDataEntry(Long id) {
		try {
			return api.getDataEntry(id);
		} catch (Exception e) {
			return null;
		}
	}

	public DataEntryDTO deleteDataEntry(Long id) {
		try {
			return api.deleteDataEntry(api_access_token, id);
		} catch (Exception e) {
			return null;
		}
	}
}
