package org.alliancegenome.loader.api;

import java.util.List;

import org.alliancegenome.api.dto.GeneDTO;
import org.alliancegenome.common.interfaces.GeneRESTInterface;

import si.mazi.rescu.RestProxyFactory;

public class GeneControllerRESTAPI {
	
	private String api_access_token;
	private GeneRESTInterface api = null;
	
	public GeneControllerRESTAPI(String api_access_token, String baseURL) {
		this.api_access_token = api_access_token;
		api = RestProxyFactory.createProxy(GeneRESTInterface.class, baseURL);
	}

	public GeneDTO createGene(GeneDTO gene) {
		try {
			return api.createGene(api_access_token, gene);
		} catch (Exception e) {
			return null;
		}
	}

	public List<GeneDTO> createGeneBatch(List<GeneDTO> genes) {
		try {
			System.out.println("Sending Batch of Genes: " + genes.size());
			return api.createGeneBatch(api_access_token, genes);
		} catch (Exception e) {
			return null;
		}
	}

	public GeneDTO updateGene(GeneDTO gene) {
		try {
			return api.updateGene(api_access_token, gene);
		} catch (Exception e) {
			return null;
		}
	}

	public List<GeneDTO> getGene(String id, String symbol, String soTermId, String taxonId, String name) {
		try {
			return api.getGene(id, symbol, soTermId, taxonId, name);
		} catch (Exception e) {
			return null;
		}
	}

	public GeneDTO deleteGene(Long id) {
		try {
			return api.deleteGene(api_access_token, id);
		} catch (Exception e) {
			return null;
		}
	}
	
}
