package org.alliancegenome.loader.files;

import java.io.File;
import java.io.IOException;

import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

public class TARFile extends DataFile {

	private String insideFileName;
	
	public TARFile(String fileName, String insideFileName) {
		super(fileName);
		super.init();
		this.insideFileName = insideFileName;
	}

	public void extract_all() {
		File extracted = new File(savePath + "/" + insideFileName);
		if(!extracted.exists()) {
			System.out.println("Extracting files from (" + fileOutputPath + ") ...");
			try {
				Archiver archiver = ArchiverFactory.createArchiver("tar", "gz");
				archiver.extract(new File(fileOutputPath), new File(savePath));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
