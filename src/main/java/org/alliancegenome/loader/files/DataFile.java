package org.alliancegenome.loader.files;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public abstract class DataFile implements DataFileInterface {

	protected String savePath = "tmp";
	protected String fileName = "";
	protected String fileOutputPath = "";
	protected boolean fileExists = false;

	public DataFile(String fileName) {
		this.fileName = fileName;
		fileOutputPath = savePath + "/" + fileName;
		init();
	}

	public DataFile(String fileName, String savePath) {
		this.fileName = fileName;
		this.savePath = savePath;
		fileOutputPath = savePath + "/" + fileName;
		init();
	}

	public void init() {
		File save = new File(savePath);
		if(!save.exists()) {
			try {
				System.out.println("Creating save path: " + savePath);
				FileUtils.forceMkdir(new File(savePath));
			} catch (IOException e) {
				System.out.println("Could not make destination for saving files: ");
				e.printStackTrace();
				System.exit(1);
			}
		}
		File f = new File(savePath + "/" + fileName);
		fileExists = f.exists();
	}
	
}
