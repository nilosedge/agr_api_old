package org.alliancegenome.loader.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TXTFile extends DataFile {

	public TXTFile(String fileName) {
		super(fileName);
	}
	
	public BufferedReader getBufferedReader() throws FileNotFoundException {
		System.out.println("Loading Data from: " + fileOutputPath);
		return new BufferedReader(new FileReader(new File(fileOutputPath)));
	}

}
