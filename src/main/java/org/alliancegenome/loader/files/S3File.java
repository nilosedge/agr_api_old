package org.alliancegenome.loader.files;


public class S3File extends WgetFile {

	public S3File(String s3bucket, String s3FileName) {
		super("https://s3.amazonaws.com/" + s3bucket + "/" + s3FileName, s3FileName);
	}

	public S3File(String s3bucket, String s3FileName, String savePath) {
		super("https://s3.amazonaws.com/" + s3bucket + "/" + s3FileName, s3FileName, savePath);
	}

	public void download() {
		if(!fileExists) {
			System.out.println("Downloading data from s3: ");
			super.download();
		}
	}

}
