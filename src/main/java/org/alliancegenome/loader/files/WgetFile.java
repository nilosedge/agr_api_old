package org.alliancegenome.loader.files;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class WgetFile extends DataFile {

	private String urlPath;

	public WgetFile(String urlPath, String fileName) {
		super(fileName);
		this.urlPath = urlPath;
		super.init();
	}

	public WgetFile(String urlPath, String fileName, String savePath) {
		super(fileName, savePath);
		this.urlPath = urlPath;
		super.init();
	}

	public void download() {
		if(!fileExists) {
			System.out.println("Downloading data from: (" + urlPath + ") ...");
			try {
				URL url = new URL(urlPath);
				FileUtils.copyURLToFile(url, new File(fileOutputPath));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
