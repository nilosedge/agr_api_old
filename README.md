# agr_api
This project tries to re-write the agr project in Java. In particular, it will replace the backend constisting of the api and the indexer part. The front-end should not be affected. 

Application Server: wildfly version 10.1.0: http://wildfly.org/downloads/
Build tool: Gradle version 3.5 

Loading Data:
The class AgrLoader loads all data files by downloading the files from the S3 bucket and then reading the mostly json-formatted files. The service layer then turns the individual entities into DTO objects that are passed on to a web service that inserts them into a postgres database. The Collection of DTOs is transformed into persistent entities, such as Gene, Orthology, etc. which are handled by Hibernate. 

Indexing: 
Indexing is happening partly concurrently with the loading to avoid unnecessary reading from the database. 
The DTO objects are then transformed into the another form of DTO that then can be inserted into Elasticsearch.
At the moment we have gene documents in ES.

API:
